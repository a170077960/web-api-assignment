const express = require("express");
const router = express.Router();

const checkAuth = require('../middleware/check_auth');
const UserController = require('../controllers/users');
const User = require("../models/user");

router.post("/signup", UserController.user_signup);

router.post("/login", UserController.user_login);

router.delete("/:userId", checkAuth, UserController.user_delete);

router.get("/", UserController.user_get);

module.exports = router;
